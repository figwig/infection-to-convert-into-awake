#include "AwakePlayer.h"


AwakePlayer::AwakePlayer(char* sourceImage, SDL_Renderer * renderer)
{
	playerSprite.SetText(renderer, sourceImage);

	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = playerSprite.GetRect().w;
	targetRect.h = playerSprite.GetRect().h;

}

AwakePlayer::~AwakePlayer()
{

}

void AwakePlayer::draw(SDL_Renderer* renderer)
{
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = playerSprite.GetRect().w;
	targetRect.h = playerSprite.GetRect().h;

	SDL_RenderCopyEx(renderer, playerSprite.GetText(), &playerSprite.GetRect(), &targetRect, 0, 0, SDL_FLIP_NONE);
}

Point2f AwakePlayer::getPos()
{
	return Point2f(pos.x, pos.y);
}

void AwakePlayer::update(KeyFlags keys, int framerate)
{

}