#pragma once
#include "GameObject.h"
#include <string>
#include <SDL.h>
#include "Sprite.h"

class AwakePlayer : public GameObject
{
public:
	AwakePlayer(char* spriteName, SDL_Renderer*);
	~AwakePlayer();


	void draw(SDL_Renderer*);
	void update(KeyFlags, int framerate);
	Point2f getPos();

	Sprite playerSprite;
	SDL_Renderer* renderer;  //the renderer to draw things on the screen
	SDL_Rect targetRect;

	Point2f pos = { 0.0f,0.0f };
};

