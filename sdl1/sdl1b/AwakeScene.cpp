#include "AwakeScene.h"
#include "AwakePlayer.h"
#include "Background.h"

AwakeScene::AwakeScene(SDL_Window* window, SDL_Renderer* renderer) : GameScene(window, renderer) 
{

	AwakePlayer* awake = new AwakePlayer("Assets\\Images\\AwakePlayer.bmp", renderer);

	addToDrawables(awake);


	background = new Background(Point2f(0.0f, 0.0f), "Assets\\Images\\AwakeBackground.bmp", renderer);

	fpsCounterObj = new fpsCounter(Point2f(10.0f, 5.0f), renderer);

	healthObj = new staticText(Point2f(10.0f, 35.0f), renderer);

	scoreObj = new staticText(Point2f(10.0f, 65.0f), renderer);

	healthObj->setPrint("Health = ");

	scoreObj->setPrint("Score = ");
	
}


AwakeScene::~AwakeScene()
{

}