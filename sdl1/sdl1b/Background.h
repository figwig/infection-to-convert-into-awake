#pragma once
#include "GameObject.h"
#include "Sprite.h"
#include <SDL.h>
#include "Point2f.h"
#include "KeyState.h"
#include <Windows.h>
#include <SDL.h>
#include <iostream>
#include <stdio.h>


class Background : public GameObject
{


public:
	Background(Point2f initialPosition, char* sourceImage, SDL_Renderer*);

	~Background();

	void update(KeyFlags keys, int framerate);

	void draw(SDL_Renderer* renderer);

	void flipvelocity(int) {};

	Point2f getPos() { return Point2f(0, 0); };

private:
	Sprite backSprite;
};

