#include "CircleVol.h"
#include "Volume.h"
#include "GameObject.h"
#include "Point2f.h"
#include "Collision.h"
#include <stdio.h>
#include <math.h>


CircleVol::CircleVol()
{

}

CircleVol::~CircleVol()
{
}

void CircleVol::_init_(float newradius, Point2f newcentre, bool ispassive)
{
	radius = newradius;
	centre = newcentre;
	passive = ispassive;

	createPoints(10.0f);
}

bool CircleVol::checkCollision(Point2f test)
{
	bool collide = false;//collision assumed not

	float xDiff = test.x - centre.x;
	float yDiff = test.y - centre.y;

	if ((xDiff*xDiff) + (yDiff*yDiff) > (radius*radius))
	{
		//not within bounding area
	}
	else
	{
		//within bounding area
		collide = true;
	}
		
	return collide;
}


Point2f *CircleVol::getPoints()
{

	return keyPoints;
}

int CircleVol::getNumberOfPoints()
{

	return actNumPoints;
}

void CircleVol::updatePos(int x,int y)
{
	centre.x = x;
	centre.y = y;
	createPoints(10.0f);

}

void CircleVol::setOwner(GameObject* newOwner)
{
	owner = newOwner;
}

GameObject * CircleVol::getOwner()
{
	return owner;
}


void CircleVol::createPoints(float spaceBetween)
{
	int count = 0;

	int factor = 2;

	//find the lowest y point, then incremenent by the pointsNo until the highest y point. Each time work out both x points
	float minY = centre.y - radius;
	float maxY = centre.y + radius;
	float currY = minY;
	float currX = 0;
	
	while (currY< maxY)
	{
		currX = centre.x-(   sqrt( (radius*radius) - ((currY - centre.y)*(currY - centre.y))) );
		keyPoints[count] = Point2f(currX, currY);
		count++;
		currX = centre.x-(  -sqrt( (radius*radius) - ((currY - centre.y)*(currY - centre.y))) );
		keyPoints[count] = Point2f(currX, currY);
		count++;
		currY += spaceBetween;
	}

	actNumPoints = count;
}

int CircleVol::getType()
{
	return 0;
}

float CircleVol::getRadius()
{
	return radius;
}

