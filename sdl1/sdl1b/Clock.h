#pragma once
class Clock
{
public:
	Clock();
	~Clock();
	void tick(int, bool);
	int getFramerate();
	int getAvgFramerate();
	float getTimePassed();
	void delayTillNextFrame();
	float FRAMERATETARGET =300.0f;

private:
	float framerate;
	bool debugb = false;
	float avgFramerate;
	float firstTimer = 0.0f;
	float secondTimer = 0.0f;
	float timePassed = 0;
	float lastTimer = 0.0f;
	float lastFramerate = 0.0f;
	float diff = 0.0f;

};

