#pragma once
#include <SDL.h>
#include "KeyState.h"

#include "Point2f.h"
class Virus;
class Bullet;
class Volume;
class GameObject
{
public:
	GameObject() {}
	~GameObject() {}

	//Pure Virtuals - every GameObject has to have their own definition!!!
	virtual void draw(SDL_Renderer*) = 0;
	virtual void update(KeyFlags, int framerate) = 0;
	virtual Point2f getPos() = 0;

	//default virtuals! Every GameObject can have their own definition, but doesn't have to
	virtual void flipvelocity(GameObject*) {}
	virtual bool doneCheck() { return deleteMe; }
	virtual int getDamage() { return 0; }
	virtual int getDamagePlayer()	{ return 0; }
	virtual int getDamageVirus()	{ return 0; }
	virtual int getDamageBullet()	{ return 0; }
	virtual Volume* getVolumePtr()  {return nullptr;}


protected:
	bool deleteMe = false;

};

