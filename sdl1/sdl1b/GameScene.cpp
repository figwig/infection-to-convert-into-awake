#include "GameScene.h"
#include <SDL.h>
#include "SMGame.h"
#include "Player.h"
#include "Background.h"
#include "Virus.h"
#include <string>
#include "Volume.h"
#include "fpsCounter.h"
#include "point.h"
#include "staticText.h"
#include "Splash.h"
#include "GameObject.h"
#include <random>

/*
Infection was written by Will Hain

using base code from SMGame by Paul Angel
which was put together from LazyFoo's SDL C++ tutorials.

The Background texture was designed by Celyn Hughes

The Player and Virus textures are freeware available online at
http://millionthvector.blogspot.co.uk/p/free-sprites.html

All other textures by Will Hain

Copyright 2018

*/

//Constructor - debugBool inside for debugging!
GameScene::GameScene(SDL_Window* window, SDL_Renderer* newRenderer) : SMScene(window, renderer) {
	
	/*
	Debug bool below!! Set for:

	Debug Points on all collision checking points
	Player co-ords printed on screen
	Slowed down game time
	Player Doesn't die
	*/
	//debugb = true;

	renderer = newRenderer;


	//Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);

	//newWaveTrigger = true;
	//currWave = 0;
	//gamePlaying = false;

	//explosion = Mix_LoadWAV("Assets\\Sounds\\explosion.wav");// FIND AN EXPLOSION SOUND EFFECT
	//
	//music = Mix_LoadMUS("Assets\\Sounds\\Monplaisir_-_05_-_Level_2.wav");
}

GameScene::~GameScene()
{
	delete player;
	delete[] drawables;
	delete background;
	delete healthObj;
	delete fpsCounterObj;
	Mix_FreeChunk(explosion);
	Mix_FreeMusic(music);
}

uint32_t GameScene::run(Clock* newGameClock) {
	gameClock = newGameClock;
	frames = 0;
	int spawnLocCount = 0;
	std::mt19937 mt(randDevice());

	Mix_PlayMusic(music, 5);
	//game loop

	while (!quitGame) {

		//Random Numbers cast for the hitMessage and spawn location
		hitMessage = message(mt);
		spawnLocCount = virus(mt);

		////If the game is ready for another wave, do it!
		//if (newWaveTrigger)
		//{
		//	startWave(currWave);
		//}

		////spawn a virus if it's time!
		//if (frameDelay <(int) gameClock->getTimePassed()- prevWavesTime)
		//{
		//	if ((int)gameClock->getTimePassed()-prevWavesTime > interval*(spawned+1))
		//	{
		//		if (levelSpawns > 0)
		//		{
		//			//Time to spawn a virus!!!
		//			spawnVirus(locs[spawnLocCount]);
		//			levelSpawns -= 1;
		//			if (spawnLocCount == 9) { spawnLocCount = 0; }
		//			spawned += 1;
		//		}
		//		else
		//		{
		//			//Wave done! Are all viruses dead?
		//			if (virusesLeft() == 0)
		//			{
		//				newWaveTrigger = true;
		//				spawned = 0;
		//				prevWavesTime = gameClock->getTimePassed();
		//			}

		//		}
		//	}
		//}

		//Check for collisions etc
		handleEvents();

		// 2. Update game state
		updateState();
		

		// 3. Draw current game state
		render();

		gameClock->tick(frames, debugb);
		frames++;
		framerate = gameClock->getFramerate();


		if ((frames % 500) == 0)
		{
			everyHundred();
		}

	}  // 4. Repeat


	return 0;
};

void GameScene::spawnVirus(Point2f pos)
{
	int count = 0;
	bool slotFound = false;
	while (!slotFound)
	{
		if (count > VIRUSMAX)
		{
			printf("Too many Viruses");
			slotFound = true;
		}

		else
		{
			if (viruses[count] == nullptr)
			{
				//found a slot
				slotFound = true;

				viruses[count] = new Virus(pos, virusImageSource, renderer);
				addToVolumes(viruses[count]->getVolumePtr());
				
				
			}
		}
		count++;
	}

}

void GameScene::createPlayerDebug()
{
	if (debugb)
	{
		playerX = new staticText(Point2f(10.0f, 65.0f), renderer);
		playerY = new staticText(Point2f(10.0f, 95.0f), renderer);
		playerX->setPrint("Player X: ");
		playerY->setPrint("Player Y: ");
	}
}

void GameScene::createDebugPoints()
{
	if (debugb)
	{


		int count = 0;
		int count2 = 0;
		Point2f newP = Point2f(0, 0);

		for each (Volume* ref in volumes)
		{
			if (ref != nullptr)
			{
				//add each keypoint to the debugppoints list, if debugmax reached then stop.
				if (count < DEBUGMAX)
				{
					int maxPoints = ref->getNumberOfPoints();

					for (count2; count2 < maxPoints; count2++)
					{
						newP = ref->getPoints()[count2];
						if ((newP.x == 0) && (newP.y == 0))
						{

						}
						else
						{
							debugPoints[count] = new  point(Point2f(newP.getX(), newP.getY()), "Assets\\Images\\point.png", renderer);
							count++;
						}
					}
					count2 = 0;

				}
			}
		}
	}

}

void GameScene::despawnWave(int waveNo)
{
	if (waveNo == 0)
	{
		//spawn like a menu
	}
	else if (waveNo == 1)
	{

	}
	else if (waveNo == 2)
	{

	}
	else if (waveNo == 3)
	{

	}

}

void GameScene::everyHundred()
{
	//printouts for debugging  that don't wanna be every frame
}

void GameScene::addToDrawables(GameObject* newGameObj)
{
	count5 = 0;
	slotD = false;
	while (!slotD)
	{	

		if (drawables[count5] == nullptr)
		{
			drawables[count5] = newGameObj;
			slotD = true;
			//if there is a volume Pointer, add that too.
			if (newGameObj->getVolumePtr() != nullptr)
			{
				addToVolumes(newGameObj->getVolumePtr());
			}

		}
		if (count5 >= DRAWABLE_MAX)
		{
			slotD = true;
			printf("Too many Drawbables\n");
		}
		count5++;
	}
}

void GameScene::addToVolumes(Volume* newVol)
{
	count6 = 0;
	slotV = false;
	while(!slotV)
	{
		if (volumes[count6] == nullptr)//if the slot isn't already full
		{
			volumes[count6] = newVol;
 			slotV = true;
		}

		if (count6 >= VOLUMEMAX-1)//preventing from walking off the array next loop
		{
			slotV = true;
			printf("Too many Volumes");
		}
		count6++;
	}
}

void GameScene::deleteVolume(Volume* vol)
{
	int count = 0;
	bool found = false;
	for each (Volume* ref in volumes)
	{
		if (!found)
		{


			if (ref == vol)
			{
				volumes[count] = nullptr;
				found = true;
			}
		}
		count++;
	}
}

void GameScene::spawnHitMessage(Point2f pos)
{
	switch (hitMessage) 
	{
	case 0:
		addToDrawables(new Splash("Nice Shot!", pos, renderer, 0.8f, red, 3, Point2f(160.0f, 20.0f), 190));
		break;
	case 1:
		addToDrawables(new Splash("Awesome!", pos, renderer, 0.8f, red, 3, Point2f(160.0f, 20.0f), 190));
		break;
	case 2:
		addToDrawables(new Splash("Sick m8!", pos, renderer, 0.8f, red, 3, Point2f(160.0f, 20.0f), 190));
		break;
	case 3:
		addToDrawables(new Splash("Radical", pos, renderer, 0.8f, red, 3, Point2f(160.0f, 20.0f), 190));
		break;
	case 4:
		addToDrawables(new Splash("Bullseye!", pos, renderer, 0.8f, red, 3, Point2f(160.0f, 20.0f), 190));
		break;
	case 5:
		addToDrawables(new Splash("Amazeballz!", pos, renderer, 0.8f, red, 3, Point2f(160.0f, 20.0f), 190));
		break;
	case 6:
		break;
	}
	

}

void GameScene::updateState(void) {

	if (gamePlaying)
	{

		//get the framerate from the clock
		framerate = gameClock->getFramerate();

		// Update player with the keypoint input and grab its position
		player->update(keyState, framerate);

		Point2f playerLoc = player->getPos();

		//update the viruses! Also inform them of the Player's location .. fix this coupling please!
		count22 = 0;
		for each (Virus* ref in viruses)
		{
			if (ref != nullptr)
			{

				float virusSpeed = (2.0f / framerate);

				ref->moveTow(playerLoc.x, playerLoc.y, virusSpeed);


				ref->update(keyState, framerate);
				if (ref->getHealth() <= 0)
				{
					spawnHitMessage(ref->getPos());
					int i = Mix_PlayChannel(-1, explosion, 0);
					score += (1000 - ref->getAge());
					printf("return %d\n", i);
					deleteVolume(ref->getVolumePtr());

					delete ref;
					viruses[count22] = nullptr;


				}
			}
			count22 += 1;
		}


		//update debug features if debug is on
		if (debugb)
		{
			count = 0;

			newP = Point2f(0, 0);
			count2 = 0;
			playerX->update(keyState, player->getPos().x);
			playerY->update(keyState, player->getPos().y);

			for each (Volume* ref in volumes)
			{
				if (ref != nullptr)
				{
					//update every point in the debugpoints list
					if ((count < DEBUGMAX))
					{	
						
						maxPoints = ref->getNumberOfPoints();

						for (count2; count2 < maxPoints; count2++)
						{
							//if a debug Point already exists - update it's pos.
							//if there are no longer enough, add more
							if (debugPoints[count] != nullptr)
							{
								newP = ref->getPoints()[count2];

								if ((newP.x == 0) && (newP.y == 0))
								{

								}
								else
								{
									debugPoints[count]->updatePos(newP);
									count++;
								}
							}
							else
							{
								newP = ref->getPoints()[count2];

								if ((newP.x == 0) && (newP.y == 0))
								{

								}
								else
								{
									debugPoints[count] = new point(newP, "Assets\\Images\\point.png", renderer);
									count++;
								}
							}
						}
						count2 = 0;

					}
				}
			}
		}


		//update framecounter with average frames, not actual frames
		fpsCounterObj->update(keyState, gameClock->getAvgFramerate());
		
		//health gets the player's health passed to it (fix coupling again please)
		healthObj->update(keyState, player->getHealth());

		//Score obj gets the player's score
		scoreObj->update(keyState, score);

	}

	//Update The Drawable List!!
	int count23 = 0;
	for (count23; count23 < DRAWABLE_MAX; count23++)
	{
		//for everything in the drawable list, if it isn't a null pointer...
		if (drawables[count23] != nullptr)
		{
			//Update it!
			drawables[count23]->update(keyState, framerate);


			//if it says its done...
			if (drawables[count23]->doneCheck())
			{

				//remove it and it's Volume (if it has one) from the arrays!
				if ((drawables[count23]->getVolumePtr() != nullptr))
				{
					deleteVolume(drawables[count23]->getVolumePtr());
				}

				//delete the object
				delete drawables[count23];
				drawables[count23] = nullptr;
			}

		}
		//done! next object please....
		count23++;
	}
}
//////////////////////////////////////////////////////Update

void GameScene::render(void) {
	//call all objects that need drawing!

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); //Prepare SDL things
	SDL_RenderClear(renderer);

	background->draw(renderer);
	if (gamePlaying)
	{

		for each (Virus* ref in viruses)
		{
			if (ref != nullptr)
			{
				ref->draw(renderer);
			}
		}

		player->draw(renderer);

		//drawing debug points
		if (debugb)
		{
			playerX->draw(renderer);
			playerY->draw(renderer);
			for each (point* ref in debugPoints)
			{
				if (ref != nullptr)
				{
					ref->draw(renderer);
				}

			}
		}

		fpsCounterObj->draw(renderer);
		healthObj->draw(renderer);
		scoreObj->draw(renderer);


		// 3. Present the current frame to the screen
		
	}
	int count24 = 0;
	for (count24; count24 < DRAWABLE_MAX; count24++)
	{
		if (drawables[count24] != nullptr)
		{
			drawables[count24]->draw(renderer);
		}
		count24++;
	}
	SDL_RenderPresent(renderer);

}///////////////////////////////////////////

//////////////////////////////////////////////////////Render

void GameScene::initGame()
{
	// Update player with key statesfor first frame
	player->update(keyState, 0);
	Point2f playerLoc = player->getPos();

	//updating each virus for first frame
	for each (Virus* ref in viruses)
	{
		if (ref != nullptr)
		{
			ref->update(keyState, 0);
		}
	}


	//creating each of the debug features

}

void GameScene::createObjects()
{
	background =  new Background(Point2f(0.0f, 0.0f), "Assets\\Images\\Background.png", renderer);

	fpsCounterObj = new fpsCounter(Point2f(10.0f, 5.0f), renderer);

	healthObj = new staticText(Point2f(10.0f, 35.0f), renderer);

	scoreObj = new staticText(Point2f(10.0f, 65.0f), renderer);

	healthObj->setPrint("Health = ");

	scoreObj->setPrint("Score = ");
}

void GameScene::handleEvents() 
{
	
	SDL_Event event2;

	// Check for next event (note this system only does one event per frame
	SDL_PollEvent(&event2);

	checkKeystates(event2);
	if (gamePlaying)
	{
		checkCollisions(event2);

		if (!checkEndGame())
		{
			//Nothing, game goes on

		}
		else
		{	
			//game finished
			gamePlaying = false;
			
			currWave = 98;
			newWaveTrigger = true;
		}
	}
	
}

bool GameScene::checkEndGame()
{
	bool end = false;
	if (!debugb)
	{

		if (player->getHealth() <= 0)
		{
			end = true;
		}
	}
	return end;
}

void GameScene::checkCollisions(SDL_Event event)
{
	int count1 = 0;
	int count2 = 0;
	int count = 0;
	//loop once for every volume checked to check it against all key points


	for (count; count< VOLUMEMAX;count++)
	{
		count2++;
		bool hit = false;
		Volume* secondVolume;
		Volume* currVolume = volumes[count];
		
		//if not nullptr, error catching

		if (currVolume != nullptr) {
			
			//loop once for each virus object currently in game
			for each (Virus * ref2 in viruses)
			{
				if (ref2 != nullptr)
				{


					//loop once for each key point of the virus
					int top = (ref2->getVolumePtr()->getNumberOfPoints());


					for (count1; count1 < top; ++count1)
					{
						Point2f point = ref2->getVolumePtr()->getPoints()[count1];

						if (currVolume->checkCollision(point))
						{
							secondVolume = ref2->getVolumePtr();

							if (currVolume != secondVolume)
							{
								takeCollisionAction(Collision(currVolume, secondVolume, point, 0.0f));
							}
						}
					}
				}

			}
			//loop through each key point of player's
			count1 = 0;
			for (count1; count1 < player->getVolumePtr()->getNumberOfPoints(); ++count1)
			{
				if (currVolume->checkCollision(player->getVolumePtr()->getPoints()[count1]))
				{
					secondVolume = player->getVolumePtr();
					//take action
					if (currVolume != secondVolume)
					{
						takeCollisionAction(Collision(currVolume, secondVolume, player->getVolumePtr()->getPoints()[count1], 0.0f));
					}

				}

			}


			//loop through each drawable object if it has a volume
			count1 = 0;
			for each (GameObject * ref3 in drawables)
			{
				if ((ref3 != nullptr) && (ref3->getVolumePtr() != nullptr))
				{
					secondVolume = ref3->getVolumePtr();
					if (currVolume != secondVolume)
					{

					//loop once for each key point of the object
					int top = (ref3->getVolumePtr()->getNumberOfPoints());


						for (count1; count1 < top; ++count1)
						{
							Point2f point = secondVolume->getPoints()[count1];

							if (currVolume->checkCollision(point))
							{
								takeCollisionAction(Collision(currVolume, secondVolume, point, 0.0f));
							}
						}
					}
				}

			}


		}
	}
}

void GameScene::takeCollisionAction(Collision theCollision)
{
	GameObject* perp = theCollision.getPrimary()->getOwner();
	GameObject* victim = theCollision.getSecondary()->getOwner();
	
	perp->flipvelocity(victim);

	victim->flipvelocity(perp);

	
}

void GameScene::checkKeystates(SDL_Event event)
{
	switch (event.type) {

		// Check if window closed
	case SDL_QUIT:

		quitGame = true;
		break;

		// Key pressed event
	case SDL_KEYDOWN:

		// Toggle key states based on key pressed
		switch (event.key.keysym.sym)
		{
		case SDLK_UP:
			keyState |= Keys::Up;
			break;

		case SDLK_DOWN:
			keyState |= Keys::Down;
			break;

		case SDLK_LEFT:
			keyState |= Keys::Left;
			break;

		case SDLK_RIGHT:
			keyState |= Keys::Right;
			break;

		case SDLK_ESCAPE:
			quitGame = true;
			break;
		case SDLK_a:
			keyState |= Keys::A;
				break;
		case SDLK_d:
			keyState |= Keys::D;
			break;
		case SDLK_SPACE:
			keyState |= Keys::Space;
			break;
		}
		break;

		// Key released event
	case SDL_KEYUP:

		switch (event.key.keysym.sym)
		{
		case SDLK_UP:
			keyState &= (~Keys::Up);
			break;

		case SDLK_DOWN:
			keyState &= (~Keys::Down);
			break;

		case SDLK_LEFT:
			keyState &= (~Keys::Left);
			break;

		case SDLK_RIGHT:
			keyState &= (~Keys::Right);
			break;
		case SDLK_a:
			keyState &= (~Keys::A);
			break;
		case SDLK_d:
			keyState &= (~Keys::D);
			break;
		case SDLK_SPACE:
			keyState &= (~Keys::Space);
			break;

		}

		break;
	}
}

int  GameScene::virusesLeft()
{
	int count = 0;
	for each (Virus* ref in viruses)
	{
		if (ref != nullptr)
		{
			count++;
		}
	}
	return count;
}