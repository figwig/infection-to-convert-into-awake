#pragma once

#include "SMScene.h"
#include "KeyState.h"
#include "Virus.h"
#include "Volume.h"
#include "textObject.h"
#include <stdlib.h>
#include <random>
#include "fpsCounter.h"
#include "point.h"
#include "staticText.h"
#include <SDL_mixer.h>

class SMGame;
class Player;
class Background;

class GameScene : public SMScene {

protected:
	// Flag bools
	bool quitGame = false;
	bool debugb = false;
	bool newWaveTrigger = true;
	bool gamePlaying = false;
	bool slotV = false;
	bool slotD = false;

	//Sounds
	Mix_Chunk* explosion = nullptr;

	Mix_Music* music = nullptr;



	//Point2f Variables
	Point2f newP;


	//Int Variables (including counts, using multiple to help with debugging)
	int frames = 0;
	int currWave = 0;
	int interval = 1000;
	int framerate = 1;
	int levelSpawns = 0;
	int frameDelay = 0;
	int spawned = 0;
	int score = 0;
	int prevWavesTime = 0;
	int count6 = 0;
	int count5 = 0;
	int count = 0;
	int count22 = 0;
	int count2 = 0;
	int maxPoints = 0;
	int hitMessage = 0;


	//distributions
	std::uniform_int_distribution<int> virus{0, 8};
	std::uniform_int_distribution<int> message{ 0, 6 };



	//Strings
	char* virusImageSource = "Assets\\Images\\enemy.png";

	//Induvidual Object Declarations
	Player*			player = nullptr;
	Clock*			gameClock = nullptr;
	fpsCounter *	fpsCounterObj = nullptr;
	Background*		background = nullptr;
	staticText*		healthObj = nullptr;
	staticText*		scoreObj = nullptr;
	staticText*		playerX = nullptr;
	staticText*		playerY = nullptr;
	Bullet*			lastBullet = nullptr;
	Bullet*			newBullet = nullptr;
	std::random_device randDevice;
	


	//SDL Prequesities
	SDL_Renderer* renderer;
	KeyFlags	keyState = 0;
	SDL_Color red = { 121, 26, 26 }; 
	SDL_Color purp ={ 118, 43,150 };

	//Point2f Array of spawnable locations for viruses
	Point2f locs[9] = {
		Point2f(33.0f,948.0f),
		Point2f(210.0f,-13.0f),
		Point2f(1214.0f,-7.0f),
		Point2f(1777.0f,955.0f),
		Point2f(1669.0f,840.0f),
		Point2f(50.0f,400.0f),
		Point2f(100.0f,670.0f),
		Point2f(630.0f,150.0f),
		Point2f(1600.0f,400.0f)
	};

	//Arrays of Objects////////////////////////////////////////////////////////////////////////////////////////
	//
	//Below! :)

	//Drawable gameobjects that are active regardless of gameplaying
	const static int DRAWABLE_MAX = 50;
	GameObject * drawables[DRAWABLE_MAX] = { nullptr };

	//Array to contain virus objects
	static const int VIRUSMAX = 50;
	Virus* viruses[VIRUSMAX] = { nullptr };

	//Queue of things to be spawned
	static const int QUEUEMAX = 50;
	Virus* queue[QUEUEMAX] = { nullptr };

	//Array to constain debugPoints
	static const int DEBUGMAX = 1024;
	point* debugPoints[DEBUGMAX] = { nullptr };

	//Array to contain Collision Volumes
	static const int VOLUMEMAX = 100;
	Volume* volumes[VOLUMEMAX] = { nullptr };

	//Object Returning methods
	

	//Various Void returning methods
	void handleEvents(void);
	void updateState(void);
	void render(void);
	void spawnVirus(Point2f);
	void checkKeystates(SDL_Event);
	void checkCollisions(SDL_Event);
	void takeCollisionAction(Collision);
	void everyHundred();
	void createObjects();
	void initGame();
	void startWave(int);
	void despawnWave(int);
	void createDebugPoints();
	void deleteVolume(Volume* vol);
	void spawnHitMessage(Point2f);
	void addToVolumes(Volume*);
	void createPlayerDebug();
	

	//int returning methods
	int virusesLeft();

	//Bool returns
	bool checkEndGame();



//Public Variables and Methods!
public:

	// Constructor - perform main game initialisation here.  Load images, sounds etc.
	GameScene(SDL_Window*	window, SDL_Renderer* renderer);
	~GameScene();

	// The main game loop.  Once run() has been called, we stay in the game loop until the 'quitGame' flag is set to true.
	uint32_t run(Clock*);

	void addToDrawables(GameObject*);

};