#include "Player.h"
#include <SDL_image.h>
#include "Sprite.h"
#include "GameObject.h"
#include <SDL.h>
#include "KeyState.h"
#include "Point2f.h"
#include "Volume.h"
#include "CircleVol.h"
#include "Sprite.h"
#include "rectVol.h"
#include "GameScene.h"

Player::Player(Point2f initialPosition, char* sourceImage, SDL_Renderer* newRenderer, GameScene* newSceneRef) {

	renderer = newRenderer;
	sceneRef = newSceneRef;
	pos = Point2f(initialPosition.x, initialPosition.y);
	theta = 0.0f;
	playerSprite.SetText(renderer, sourceImage);
	currentSpeed = Point2f(0, 0);


	playerSprite.SetRect(0, 0, 173, 291);


	SDL_Rect targetRect;

	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = playerSprite.GetRect().w * SIZE;
	targetRect.h = playerSprite.GetRect().h * SIZE;

	Point2f actCent = Point2f(pos.x + ((targetRect.w)/2), pos.y + ((targetRect.h)/2));

	playerVolume._init_(playerSprite.GetRect().h*SIZE, playerSprite.GetRect().w*SIZE, actCent  , false, theta);

	if (debugb)
	{
		debug = new staticText(Point2f(60.0f, 90.0f), renderer);
		debug2 = new staticText(Point2f(60.0f, 110.0f), renderer);
	}
	
	playerVolume.setOwner(this);
}


Player::~Player() {

	// Clean-up resources
}

Volume*  Player::getVolumePtr()
{

	return (Volume*) &playerVolume;
}


void Player::flipvelocity(GameObject* ref)
{

	health -= ref->getDamagePlayer();
	if (ref->getDamagePlayer() != 0)
	{
		currentSpeed.x = 0;
		currentSpeed.y = 0;
	}
	else
	{

	}
}

void Player::draw(SDL_Renderer* renderer) {


	// Derived value
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = playerSprite.GetRect().w* SIZE;
	targetRect.h = playerSprite.GetRect().h* SIZE;
	Point2f actCent = Point2f(pos.x + ((targetRect.w)/2), pos.y + ((targetRect.h)/2));
	
	playerVolume._init_(playerSprite.GetRect().h*SIZE*VOLUMEMOD,playerSprite.GetRect().w*VOLUMEMOD*SIZE,actCent, false, theta);


	if (debugb)
	{
		debug->setPrint("X is: ");
		debug->draw(renderer);
		debug2->setPrint("Y is: ");
		debug2->draw(renderer);

	}
	SDL_RenderCopyEx(renderer, playerSprite.GetText(), &playerSprite.GetRect(), &targetRect, theta, 0, SDL_FLIP_NONE);
}

Point2f Player::getPos()
{
	return Point2f(pos.x, pos.y);
}

float Player::speedScale(Point2f currentSpeed, float theta)
{
	return sqrt((currentSpeed.x*currentSpeed.x) + (currentSpeed.y*currentSpeed.y));
}

void Player::Shoot()
{

	readyFire = false;
	sceneRef->addToDrawables( new Bullet(pos, "Assets\\Images\\Bullet.png", renderer, theta));
	bulletDelay = 5;


}



