
#include "Point2f.h"


Point2f::Point2f() {

	x = y = 0.0f;
}


Point2f::Point2f(const float initX, const float initY) {

	x = initX;
	y = initY;
}


// Accessor methods
float Point2f::getX() {

	return x;
}


float Point2f::getY() {

	return y;
}


void Point2f::setX(const float value) {

	x = value;
}


void Point2f::setY(const float value) {

	y = value;
}
