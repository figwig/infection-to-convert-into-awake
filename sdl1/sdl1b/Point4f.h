#pragma once
class Point4f
{
public:
	Point4f(float,float,float,float);
	~Point4f();
	float		x, y, w, h;


	// Accessor methods - For this implementation, we'll provide accessors, but since x, y accept all float values we don't need validation so use of accessors is optional.  (x, y) can be accessed directly from the caller since they're declared as public above
	float getX() {return x;}

	float getY() { return y; }
	void setX(const float value) { x = value; }
	void setY(const float value) { y - value; }
};

