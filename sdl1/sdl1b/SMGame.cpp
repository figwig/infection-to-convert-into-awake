
#include "SMGame.h"
#include "SMScene.h"
#include "GameScene.h"
#include <iostream>
#include "AwakeScene.h"

using namespace std;


SMGame::SMGame(Uint32 initFlags, char *windowTitle, int w, int h, Uint32 windowFlags) {


	_init_(initFlags, windowTitle,  w,  h, windowFlags);

	// Setup scenes
	scenes[0] = new AwakeScene(gameWindow, gameRenderer);



}

SMGame::~SMGame() {

	SDL_Quit();
}


void SMGame::start(void) {

	scenes[0]->run(gameClock);

}

	void SMGame::_init_(Uint32 initFlags, char *windowTitle, int w, int h, Uint32 windowFlags)
{

		const int SDL_OKAY = 0;

		int sdlStatus = SDL_Init(initFlags);

		if (sdlStatus != SDL_OKAY)
			throw "SDL init error";

		gameWindow = SDL_CreateWindow(windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, windowFlags);

		// if the window creation succeeded create our renderer
		if (!gameWindow)
			throw "Window error";


		// Enumerate render drivers
		int n = SDL_GetNumRenderDrivers();

		cout << "Number of renderers = " << n << endl;

		for (int i = 0; i < n; ++i) {

			SDL_RendererInfo renderInfo;

			if (SDL_GetRenderDriverInfo(i, &renderInfo) == 0) { // 0 means success

				cout << "Renderer " << i << " : " << renderInfo.name << endl;
			}
		}

		gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);


		// Get and report version info
		SDL_version compiled;
		SDL_version linked;

		SDL_VERSION(&compiled);
		SDL_GetVersion(&linked);
		printf("Compiled against SDL version %d.%d.%d ...\n",
			compiled.major, compiled.minor, compiled.patch);
		printf("Linking against SDL version %d.%d.%d.\n",
			linked.major, linked.minor, linked.patch);
		gameClock = new Clock();

}