#pragma once

#include <SDL.h>
#include <cstdint>
#include "Clock.h"


// SMScene is an abstract class that represents a part of the game (the main menu, a game level, intro sequence etc).  We never instantiate (create objects of) SMSCene.  What we do is create what are called "concrete subclasses" that represent actual in-game sections.  SMScene exists as useful class to relate these different scenes together.
class SMScene {

private:

	// Although we SMGame object owns the main game window and renderer objects, we still need these in each scene (so we can actually draw something).  Therefore we'll store references in SMScene also.  These are passed to the SMScene constructor so each scene object knows about the game window and renderer from the moment its created!
	SDL_Window*		gameWindow = nullptr;
	SDL_Renderer*	gameRenderer = nullptr;

public:

	// Constructor
	SMScene(SDL_Window*	window, SDL_Renderer* renderer) : gameWindow(window), gameRenderer(renderer) {}

	// Accessor methods
	SDL_Window* getWindow() {

		return gameWindow;
	}

	SDL_Renderer* getRenderer() {

		return gameRenderer;
	}

	// Interface sub-classes need to override

	// Note: This method is 'pure virtual' - it's C++'s way of declaring an abstract class (other languages let us use the 'abstract' keyword in the class declaration) and being pure virtual we don't actually provide an implementation for the run() method.  Why not?  SMScene, as noted above, is intended to be subclassed with scene classes that actually do something specific (the main menu, level 1 etc) - these provide their own version of run which does what's needed.
	virtual uint32_t run(Clock*) = 0;
};
