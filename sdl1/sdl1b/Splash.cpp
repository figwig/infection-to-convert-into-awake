#include "Splash.h"


Splash::Splash(char* newText, Point2f initialPosition, SDL_Renderer* renderer, float seconds, SDL_Color colour, int newAlphaPerSec, Point2f sizeM, int newPeakTrans)
{
	TTF_Init();
	text = newText;
	peakTrans = newPeakTrans;
	Sans = TTF_OpenFont("Assets\\Fonts\\outwrite.ttf", 24);

	pos = Point2f(initialPosition.x, initialPosition.y);

	White = colour;
	timeOn = seconds;

	sixth = ((timeOn * 1000) / 6);

	Message_rect.x = pos.x;
	Message_rect.y = pos.y;
	Message_rect.w = sizeM.x;
	Message_rect.h = sizeM.y;
	currTrans = 0;
	alphaPerSec = newAlphaPerSec;
}

Splash::~Splash()
{
	delete(surfaceMessage);
	delete(Message);

}

void  Splash::update(KeyFlags keys, int framerate)
{
	if (firstb)
	{
		first = SDL_GetTicks();
		firstb = false;
	}
	timePassed = SDL_GetTicks() - first;

	//creating the surface from the text provided
	surfaceMessage = TTF_RenderText_Solid(Sans, text, White);

	if ((sixth >= timePassed))
	{
		//grow trans

		if (currTrans>peakTrans)
		{
			currTrans = peakTrans;
		}
		currTrans += alphaPerSec;
		SDL_SetSurfaceAlphaMod(surfaceMessage, (currTrans));

	}
	else if (timePassed<=  (sixth * 5))
	{

		SDL_SetSurfaceAlphaMod(surfaceMessage, peakTrans);
	}
	else if ((sixth * 5) <= timePassed)
	{
		//shrink transparancey
		currTrans -= alphaPerSec;

		if (currTrans<0)
		{
			currTrans = 1;
		}

		SDL_SetSurfaceAlphaMod(surfaceMessage, currTrans);

	}
	else if ( timePassed>=(sixth*6))
	{
		done = true;
		SDL_SetSurfaceAlphaMod(surfaceMessage, 1);
		
	}
	else
	{

	}



	if (timePassed > (timeOn * 1000))
	{
		done = true;
	}

	//grow a little
	if (flipflop)
	{
		flipflop = false;
	}
	else
	{
		Message_rect.h += 2;
		Message_rect.w += 2;
		Message_rect.x -= 1;
		Message_rect.y -= 1;
		flipflop = true;
	}



	


}
