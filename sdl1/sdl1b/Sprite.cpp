#include "Sprite.h"
#include <SDL_image.h>


Sprite::Sprite()
{
	position.x = 0;
	position.y = 0;
}


Sprite::~Sprite()
{
	SDL_DestroyTexture(texture);
}

SDL_Rect Sprite::GetRect()
{
	return position;
}

void Sprite::SetRect(int x, int y, int w, int h)
{
	position.x = x;
	position.y = y;
	position.w = w;
	position.h = h;
}

SDL_Texture* Sprite::GetText()
{
		return texture;
}



void Sprite::SetText(SDL_Renderer *renderer, char *sourceImage)
{
	SDL_Surface* image = IMG_Load(sourceImage);
	texture = SDL_CreateTextureFromSurface(renderer, image);

	SDL_QueryTexture(texture, 0, 0, &(position.w), &(position.h));
	

	SDL_FreeSurface(image);
}






