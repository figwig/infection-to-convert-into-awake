#include "Virus.h"



Virus::Virus(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer)
{
	pos = Point2f(initialPosition.x, initialPosition.y);
	theta = 0.0f;
	virusSprite.SetText(renderer, sourceImage);

	float x = (pos.x + ((virusSprite.GetRect().w * SIZE) / 2.0f));
	float y = (pos.y - ((virusSprite.GetRect().w * SIZE) / 2.0f));

	centrePos = Point2f(x,y);

	targetRect;

	// Derived value
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = virusSprite.GetRect().w * SIZE;
	targetRect.h = virusSprite.GetRect().h * SIZE;

	virusVolume._init_( ((virusSprite.GetRect().w * SIZE)/2) , centrePos, false);

	virusVolume.setOwner(this);


	collided = false;

}


Virus::~Virus()
{
	virusSprite.~Sprite();
	virusVolume.~CircleVol();
}

void Virus::update(KeyFlags keys, int newFramerate)

{
	framerate = newFramerate;
	theta += 0.5f;
}


Volume*  Virus::getVolumePtr()
{

	return(Volume*) &virusVolume;
}


void Virus::flipvelocity(GameObject *ref)
{
	health -= ref->getDamageVirus();
}



void Virus::draw(SDL_Renderer * renderer)
{
	age += 1;
	// Derived value
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = virusSprite.GetRect().w * SIZE;
	targetRect.h = virusSprite.GetRect().h * SIZE;

	float x = (pos.x + ((targetRect.w) / 2.0f));
	float y = (pos.y + ((targetRect.w) / 2.0f));

	centrePos = Point2f(x, y);

	virusVolume.updatePos( centrePos.x, centrePos.y);
	virusVolume.setOwner(this);

	SDL_RenderCopyEx(renderer, virusSprite.GetText(), &virusSprite.GetRect(), &targetRect, theta, 0, SDL_FLIP_HORIZONTAL);

	if (collided == true)
	{
		count += 1;
		if (count == 10)
		{
			collided = false;
			count = 0;
		}

	}
}

void Virus::move(float x, float y)
{
	pos.x += x;
	pos.y += y;
}

void Virus::moveTow(float x, float y, float speed)
{
	float xDist = x- pos.x;
	float yDist = y -pos.y;

	if (collided)
	{
		//Don't move
	}
	else
	{
	move((xDist * 0.003f), (yDist * 0.003f));
	}


}


Point2f Virus::getPos()
{
	return Point2f(pos.x, pos.y);
}

void Virus::letMove()
{
	collided = false;
}
