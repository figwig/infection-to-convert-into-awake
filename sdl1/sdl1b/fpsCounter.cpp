#include "fpsCounter.h"
#include "textObject.h"
#include "GameObject.h"
#include <SDL_ttf.h>
#include <stdlib.h>
#include "GameObject.h"
#include <cstdlib>
#include <string.h>
#include "Point2f.h"
#include <stdio.h>
#include <string>
#include <SDL_image.h>

fpsCounter::fpsCounter(Point2f initialPosition, SDL_Renderer* renderer)
{
	TTF_Init();
	Sans = TTF_OpenFont("Assets\\Fonts\\OpenSans-Regular.ttf", 24); //this opens a font style and sets a size

	pos = Point2f(initialPosition.x, initialPosition.y);

	White = { 50, 205, 50 };
}
fpsCounter::~fpsCounter()
{
	free(surfaceMessage);
	free(Message);
}

void fpsCounter::update(KeyFlags keys, int framerate)
{
	//getting the fps into a string format so it can be turned into a texture
	printOut = "FPS = ";
	printOut = printOut + std::to_string(framerate);
	cstr = new char[printOut.length() + 1];
	strcpy(cstr, printOut.c_str());

	surfaceMessage = TTF_RenderText_Solid(Sans, cstr, White);

	free(cstr);
}

void fpsCounter::draw(SDL_Renderer * renderer)
{
	Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);

	SDL_Rect Message_rect;
	Message_rect.x = pos.x;
	Message_rect.y = pos.y;
	Message_rect.w = 135;
	Message_rect.h = 30;


	SDL_RenderCopyEx(renderer, Message, NULL, &Message_rect, 0, 0, SDL_FLIP_NONE);
}


