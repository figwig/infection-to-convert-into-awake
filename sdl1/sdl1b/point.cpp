#include "point.h"

point::point(Point2f initialPosition, char* sourceImage, SDL_Renderer* renderer)
{
	pointSprite.SetText(renderer, sourceImage);

	pos = initialPosition;
}


point::~point()
{
}



void point::update(KeyFlags, int framerate)
{

}

void point::flipvelocity(int)
{
}

Point2f point::getPos()
{
	return pos;
}


void point::draw(SDL_Renderer * renderer)
{

	SDL_Rect targetRect;
	targetRect.w = 10 * SIZE;
	targetRect.h = 10 * SIZE;
	targetRect.x = int(pos.x)- targetRect.w/2;
	targetRect.y = int(pos.y) - targetRect.h/2;

	SDL_RenderCopyEx(renderer, pointSprite.GetText(), &pointSprite.GetRect(), &targetRect, 0, 0, SDL_FLIP_HORIZONTAL);
}

void point::updatePos(Point2f newPos)
{
	pos = newPos;
}