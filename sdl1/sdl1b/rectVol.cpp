#include "rectVol.h"
#include <stdio.h>
#include <math.h>
#define PI 3.14159265


rectVol::rectVol()
{
	
}


rectVol::~rectVol()
{
	delete[] keyPoints;
}

void rectVol::_init_(float newheight, float newwidth, Point2f newcentre, bool isPassive, float newDelta)
{
	width = newwidth;
	height = newheight;
	centre = newcentre;
	passive = isPassive;
	delta = newDelta;

	if (passive)
	{
		printf("Bullet Volume created\n");
	}
	createPoints(15);

}

bool rectVol::checkCollision(Point2f test)
{
	bool collide = false;//collision assumed not

	float xDiff = 0;
	float yDiff = 0;

	if (test.x > centre.x)
	{
		xDiff = test.x - centre.x;
	}
	else 
	{
		xDiff =  centre.x- test.x;
	}

	if (test.y > centre.y)
	{
		yDiff = test.y - centre.y;
	}
	else
	{
		 yDiff = centre.y - test.y;
	}


	if (  xDiff>(width/2)  |  (  yDiff>(height/2)  )  )
	{
		//not within bounding area
	}
	else
	{
		//within bounding area
		collide = true;
	}

	return collide;
	
}


Point2f * rectVol::getPoints()
{

	return keyPoints;
}

void rectVol::createPoints(float noOfPoints)
{


	delta -= 180;
	delta = ((delta*PI)/180);
	
	//work out distance between each point
	float circum = (width * 2) + (height * 2);
	int pointsDiff = circum / noOfPoints;

	//get the top left point
	newX = centre.x + ((width / 2) * cos(delta)) - ((height / 2)* sin(delta));
	newY = centre.y + ((width/2) *sin(delta)) + ((height/2 )*cos(delta)) ;

	Point2f start = Point2f(newX, newY);
	keyPoints[0] = start;
	bool done = false;
	int count = 1;
	int currside = 0;
	float cirDone = 0;
	float factor = 0.0f;
	int sideCount = 0;
	int maxSide;
	maxSide = (height / pointsDiff);
	sideCount = -maxSide;

	while (!done)
	{
		// travel down until height is reached, making points each time pointsdiff is equal. then go to the next side etc

		//first height
		if (currside == 0)
		{	
			if ((sideCount ) > maxSide)
			{
				maxSide = (width / pointsDiff);
				sideCount = -maxSide;
				currside = 1;
			}
			else
			{
				//finding all the points along the first height
				factor = pointsDiff/ height;
				// Vectors from the centre to the edge
				newX = centre.x + ((width / 2) * cos(delta));
				newY = centre.y + ((width / 2) * sin(delta));

				//how far along the edge
				newX = (newX - ( sideCount * (factor* ((height / 2)* sin(delta)))));
				newY = (newY + ( sideCount * (factor* ((height / 2)* cos(delta)))));
				
				sideCount += 1;

				cirDone += pointsDiff;
			}
		}
		
		//first width
		else if (currside == 1)
		{
			if ((sideCount) > maxSide)
			{
				maxSide = (height / pointsDiff);
				sideCount = -maxSide;
				currside = 2;
			}
			else
			{
				factor = pointsDiff / width;
				// to the edge

				newX = centre.x - ((height / 2) * sin(-delta));
				newY = centre.y - ((height / 2) *cos(-delta));

				//how far along the edge
				newX = (newX + sideCount * (factor* ((width / 2)* cos(-delta))));
				newY = (newY - sideCount * (factor*((width / 2)*sin(-delta))));

				sideCount += 1;

				cirDone += pointsDiff;
			}
		}
		//second height edge
		else if (currside == 2)
		{
			if ((sideCount) > maxSide)
			{
				maxSide = (width / pointsDiff);
				sideCount = -maxSide;
				currside = 3;
			}
			else
			{
				factor = pointsDiff / height;
				// to the edge
				newX = centre.x - ((width / 2) * cos(delta));
				newY = centre.y - ((width / 2) * sin(delta));

				//how far along the edge
				newX = (newX - (sideCount * (factor* ((height / 2)* sin(delta)))));
				newY = (newY + (sideCount * (factor* ((height / 2)* cos(delta)))));

				sideCount += 1;

				cirDone += pointsDiff;
			}
		}
		else if (currside == 3)
		{
			if ((sideCount) > maxSide)
			{
				//Done!!!
			}
			else
			{
				factor = pointsDiff / width;
				// to the edge

				newX = centre.x + ((height / 2) * sin(-delta));
				newY = centre.y + ((height / 2) *cos(-delta));

				//how far along the edge
				newX = (newX + sideCount * (factor* ((width / 2)* cos(-delta))));
				newY = (newY - sideCount * (factor*((width / 2)*sin(-delta))));

				sideCount += 1;

				cirDone += pointsDiff;
			}
		}
		else
		{
		}

		keyPoints[count] = Point2f(newX, newY);

		//troubleshooting cap of 50 points
		if (count == 50)
		{
			done = true;
		}
		count += 1;
	}

	actNumPoints = count+1;
}

int rectVol::getType()
{
	return 1;
}

float rectVol::getWidth()
{
	return width;
}

float rectVol::getHeight()
{
	return height;
}

int rectVol::getNumberOfPoints()
{
	return actNumPoints;
}

void rectVol::updatePos(int x,int y)
{
	centre.x = x;
	centre.y = y;
}

void rectVol::setOwner(GameObject* newOwner)
{
	owner = newOwner;
}

GameObject * rectVol::getOwner()
{
	return owner;
}
