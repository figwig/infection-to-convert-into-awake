#pragma once
#include "textObject.h"
class staticText :
	public textObject
{
public:
	staticText(Point2f initialPosition, SDL_Renderer* renderer);
	~staticText();

	void update(KeyFlags, int);
	void draw(SDL_Renderer*);
	Point2f				pos;
	void flipvelocity(int) {};

	Point2f getPos() { return Point2f(0, 0); };
	void setPrint(char* text);
private:
	SDL_Texture* Message;
	SDL_Surface* surfaceMessage;
	SDL_Color White;
	TTF_Font* Sans;
	std::string printOut;
	std::string tempPrintOut;
	char *cstr;
	SDL_Rect Message_rect;
};

