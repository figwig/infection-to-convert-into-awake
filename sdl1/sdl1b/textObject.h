#pragma once
#include "GameObject.h"
#include <SDL_ttf.h>
#include <stdlib.h>
#include "GameObject.h"
#include <cstdlib>
#include <string>
#include <string.h>
#include "Point2f.h"

class textObject : public GameObject
{
public:
	textObject(Point2f,  SDL_Renderer*);
	~textObject();
	textObject();
	void update(KeyFlags, int);
	void draw(SDL_Renderer*);
	Point2f				pos;
	void flipvelocity(int) {}
	Point2f getPos() { return pos; }

protected:
	SDL_Texture* Message;
	SDL_Surface* surfaceMessage;
	SDL_Color White;
	TTF_Font* Sans;
	SDL_Rect Message_rect;
};

